# FOSS on mobile devices devroom

We are happy to once again announce
that the FOSS on mobile devices Devroom
is taking place at FOSDEM 25 and
are calling you for participation!

## Important dates

- 2024-10-30 - Call for Proposals opened
- 2024-12-01 - Submission deadline
- 2024-12-10 - Acceptance notification
- 2024-12-15 - FOSDEM Schedule published
- 2025-02-01 - Devroom taking place on the second half of Saturday 

All deadline times are 23:59 UTC.

## What is this devroom about?

This devroom is about everything related to
Linux and Free Software on smartphones and other non-x86
mobile devices that can run a (close to) mainline Linux
kernel and fully FLOSS userspace.
Things that we usually take for granted on our x86 laptops and servers.


While a few people have been "daily driving"
their "Linux first smartphones" for multiple years
it is fair to say that the FOSS on Mobile ecosystem
in it's current state is not ready yet for "the masses".

In the meantime we welcome technologies
allow running Android Apps under Linux
or a Linux user space on Android kernels.

However, the authors of this CfP feel
that this can only be an interim solution
and optimistically hope for a bright future
where all vendors provide wonderful mainline support
and we expand on an ever growing software ecosystem
that puts the Android/iOS duopoly to shame ;)

## Topics sought

FOSS on Mobile really is a whole universe ranging
Some topics of interest including, but not limited to:

- Mainline device support and hardware enablement
  (kernel and bootloader development)
- Software Distribution for mobile devices
- Graphical shells
  (e.g. Phosh, Plasma Mobile, Sxmo)
- End-user applications and 
  the broader mobile Linux software ecosystem 
- Software Freedom and Firmware
- Open Source Hardware, Firmware and Software Freedom
- Sustainability, Community, Getting engaged

If you are unsure whether your topic fits,
feel free to reach out to us at <mobile-devroom-manager@fosdem.org>.

Please also take note of the CfP of
the more general Embedded, Mobile and Automotive Devroom[0]!

[0] https://lists.fosdem.org/pipermail/fosdem/2024q4/003575.html

## Submission details

We offer the following formats/time slots this year:

- lightning talk: 10 minutes; 
  ideal for quickly introducing a project, 
  giving a short status update etc
- regular slot: 20 minutes;
  the default
- long slot: 30 minutes;
  ideal for in-depth presentation or BoFs
  (please explain in your submission why you need/prefer a long slot)

**All times given include Q&A!**

Requests for shorter time slots are generally more likely to be accepted,
as we would like to offer a stage for many projects:
It is our hope that the varied backgrounds and perspectives
foster cross-project pollination.

Please use the following URL to submit a talk to the
pretalx conference system for FOSDEM 2025:
<https://fosdem.org/submit>

You can reuse your account from last year.

- Choose the "FOSS on mobile devices" track
- Please don't submit to multiple tracks, 
  rather try choosing the most appropriate on and/or 
  reach out to us.
- Fill out the form: 
  **Please note** that most information
  such as abstract and description
  will become available on the FOSDEM homepage
  (if your submission gets accepted)
- Uploading a photograph and writing a short biography is encouraged.
- Please mention the expected duration of your talk, including Q&A.


## Practical information

- As the event is hosted by FOSDEM the CoC applies [0].
- While there are no mandatory measures against COVID-19 in place at FOSDEM [1],
  we recommend masking indoors and will likely equip the room with CO2 meters
- If you have any other questions, 
  do not hesitate to contact the devroom managers via email [2]


[0] https://fosdem.org/2025/practical/conduct/
[1] https://fosdem.org/2025/practical/covid/
[2] <mobile-devroom-manager@fosdem.org>

## Licensing

This Call for Proposals work is licensed under
[CC-BY-SA 4.0](http://creativecommons.org/licenses/by/4.0/).
